---
layout: episode
title: "block styles"
teaching: 5
exercises: 10
questions:
- "This is a key question"
- "This is a secondary question"
objectives:
- "This is the first major objective."
keypoints:
- "This is a key point to take away."
---



> ## Prerequisites
>
> This is a pre-requisites block quote
{:.prereq}


## Source Code
```python
import x
for i in range(1,2):
    print(i)
```
{:.source}

## Output from Code
~~~
1
~~~
{:.output}

## Error from Code
~~~
  File "<input>", line 1
    print i
          ^
SyntaxError: Missing parentheses in call to 'print'
~~~
{:.error}


> ## Title of Callout
>
> This is what needs to be called out
>
{: .callout}


> ## Title of Challenge
>
> This is the challenge
> What will this code block do ?
>
> ```bash
> for i in x:
>   do thing 
> done
> ```
>
{: .challenge}


> ## This is a Checklist
> - This is the first
> - This is the second
{: .checklist}


> ## Challenge With A Solution
>
> The gauntlet has been thrown down !
>
> ```bash
> :(){ :|: & };:
> ```
> {: .source}
>
> > ## Solution
> >
> > You should of course not run this code
> >
> > ```bash
> > echo "Hello World Instead!" 
> > ```
> > {: .output}
> {: .solution}
{: .challenge}


> ## Lets Have A Discussion
>
> How fast would an unladen swallow fly ?
>
{: .discussion }