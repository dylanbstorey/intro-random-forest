import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.tree import DecisionTreeClassifier,export_graphviz
import graphviz
from sklearn.datasets import load_iris
import os
import random
from PIL import Image
import imageio

sns.set(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})

# Get Data In
iris = load_iris()
iris_df = pd.DataFrame(data= np.c_[iris['data'], iris['target']],
                     columns= iris['feature_names'] + ['target'])



pal = sns.cubehelix_palette(10, rot=-.25, light=.7)


def generate_joy_plot(df,name):
    """
    Generate a joy plot from a dataframe
    :param df:
    :param name:
    :return:
    """

    #drop target variable, melt, and rename column names
    df=df.drop(columns='target', axis=1).melt()
    df = df.rename(index=str, columns={'variable': 'g', 'value': 'x'})


    g = sns.FacetGrid(df, row="g", hue="g", aspect=5, height=1, palette=pal)

    # Draw the densities in a few steps
    g.map(sns.kdeplot, "x", clip_on=False, shade=True, alpha=1, lw=1.5, bw=.2)
    g.map(sns.kdeplot, "x", clip_on=False, color="w", lw=2, bw=.2)
    g.map(plt.axhline, y=0, lw=2, clip_on=False)

    # Set the subplots to overlap
    g.fig.subplots_adjust(hspace=-.25)

    # Remove axes details that don't play well with overlap
    g.set_titles("")
    g.set(yticks=[])
    g.despine(bottom=True, left=True)

    # Define and use a simple function to label the plot in axes coordinates
    def label(x, color, label):
        ax = plt.gca()
        ax.text(0,.8, label, fontweight="bold", color=color,
                ha="left", va="center", transform=ax.transAxes, fontsize=8)


    g.map(label, "x")

    # Set the subplots to overlap
    g.fig.subplots_adjust(hspace=-.25)

    # Remove axes details that don't play well with overlap
    g.set_titles("")
    g.set(yticks=[])
    g.despine(bottom=True, left=True)

    figname = '{}.png'.format(name)
    plt.savefig(figname)
    return figname


def generate_decision_tree(df,name):
    """
    Generate a decision tree from a dataframe
    :param df:
    :param name:
    :return:
    """
    model = DecisionTreeClassifier()
    X = df.drop(columns='target',axis=1)

    y = df['target']


    model.fit(X,y)

    dot = export_graphviz(model,
                          out_file=None,
                          feature_names=X.columns,
                          class_names=iris.target_names,
                          filled=True,
                          impurity=None,
                          )

    graph = graphviz.Source(dot)
    graph.format ='png'
    graph.render("{}".format(name))
    os.unlink(name)
    return "{}.png".format(name)


#full model generation
generate_joy_plot(iris_df,'_episodes/images/iris_full_joy_plot')
generate_decision_tree(iris_df,'_episodes/images/iris_full_dt')

#generate sub models
saved_files = []
dt_files = []
jp_files = []


for i in range(0,50):
    df_sample = iris_df.sample(n=100)
    features = iris.feature_names
    selected_features = random.sample(features, 1)
    for f in selected_features:
        df_sample[f] = np.nan

    jp = generate_joy_plot(df_sample,'{}_joy'.format(i))
    jp_files.append(jp)

    df_sample = df_sample.drop(columns=selected_features[0], axis=1)
    dt = generate_decision_tree(df_sample,'{}_dt'.format(i))
    dt_files.append(dt)




    dt_rw = Image.open(dt)


    dt_rw = dt_rw.resize((500,400),Image.LANCZOS)
    dt_rw.save(dt,'PNG')

    images = [Image.open(f) for f in  [jp,dt]]


    w,h = zip(*(i.size for i in images))

    w_t = sum(w)
    h_t = h[0]

    new_image = Image.new('RGB', (w_t,h_t),color=(255,255,255,0))

    x_offset=0
    for image in images:
        new_image.paste(image, (x_offset,0))
        x_offset += image.size[0]

    new_image.save('{}_combined.png'.format(i),"PNG")
    os.unlink(jp)
    os.unlink(dt)
    saved_files.append('{}_combined.png'.format(i))

with imageio.get_writer('../images/iris_sampling.gif', mode='I',duration=1) as writer:
    for f in saved_files:
        image = imageio.imread(f)
        writer.append_data(image)
        os.unlink(f)
