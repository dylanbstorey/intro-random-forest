"""
    generate_bagging_illustration.py
    ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

    A script that generates an image and gif that show the results of a bagging proceedure when applied to data.

"""
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import os
import imageio
import random

sns.set(style="white", rc={"axes.facecolor": (0, 0, 0, 0)})

# Create the data
rs = np.random.RandomState(1979)
x = rs.randn(500)
g = np.tile(list("ABCDEFGHIJ"), 50)
df = pd.DataFrame(dict(x=x, g=g))
m = df.g.map(ord)
df["x"] += m


def draw_graph(df, n):
    # Initialize the FacetGrid object
    pal = sns.cubehelix_palette(10, rot=-.25, light=.7)
    g = sns.FacetGrid(df, row="g", hue="g", aspect=15, height=.5, palette=pal)

    # Draw the densities in a few steps
    g.map(sns.kdeplot, "x", clip_on=False, shade=True, alpha=1, lw=1.5, bw=.2)
    g.map(sns.kdeplot, "x", clip_on=False, color="w", lw=2, bw=.2)
    g.map(plt.axhline, y=0, lw=2, clip_on=False)

    # Set the subplots to overlap
    g.fig.subplots_adjust(hspace=-.25)

    # Remove axes details that don't play well with overlap
    g.set_titles("")
    g.set(yticks=[])
    g.despine(bottom=True, left=True)

    file_save = '{}.png'.format(n)
    plt.savefig(file_save)
    return file_save

saved_files = []
for n in range(0, 25):
    df_sample = df.sample(n=300)
    features = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J']
    selected_features = random.sample(features, 3)
    for f in selected_features:
        df_sample['x'][df.g == f] = np.nan
    saved_files.append(draw_graph(df_sample, n))

with imageio.get_writer('../images/sampling.gif', mode='I',duration=1) as writer:
    for f in saved_files:
        image = imageio.imread(f)
        writer.append_data(image)
        os.unlink(f)

draw_graph(df,'../images/total_data')