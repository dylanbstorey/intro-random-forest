import angreal

import os
from distutils.spawn import find_executable
import re
import subprocess
import tempfile


from angreal.integrations.git import Git




@angreal.command()
@angreal.argument('episode_name')
def angreal_cmd(episode_name):
    """
    create a new episode for the lesson
    """

    episode_dir = os.path.join(os.path.dirname(__file__) , '..' , '_episodes')

    if not os.path.isdir(episode_dir):
        exit("{} doesn't appear to exist !".format(episode_dir))

    episode_regular_expression = re.compile('^(\d+)-.*\.md$')
    current_episodes = [ episode_regular_expression.match(f).group(1) for f in os.listdir(episode_dir) if episode_regular_expression.match(f)]
    current_episodes.sort(reverse=True)

    this_episode_number = increment_episode(current_episodes[0])

    this_episode_name = episode_name.replace('_','-')

    this_episode_name = os.path.join(episode_dir,
                                     '-'.join([this_episode_number,this_episode_name]) + '.md')

    (fd ,path) = tempfile.mkstemp()
    with open(fd, 'w') as default:
        print('''---
layout: episode
title: "{}"
teaching: 5
exercises: 10
questions:
- "This is a key question"
- "This is a secondary question"
objectives:
- "This is the first major objective."
keypoints:
- "This is a key point to take away."
---
        '''.format(episode_name.replace('_',' ')), file=default)

    editor = get_editor()
    if editor:
        subprocess.call('{} {}'.format(editor,path),shell=True)

    with open(this_episode_name,'a') as dst:
        with open(path,'r') as src:
            print(src.read(),file=dst)

    git = Git()
    git.add(this_episode_name)
    git.commit('-am', 'Starting work on {}'.format(os.path.basename(this_episode_name)))

    os.unlink(path)




def increment_episode(x):
    """
    increment a zero padded number by one
    """
    return str(int(x) + 1).zfill(len(x))

def get_editor():
    """
    get the system text editor
    """
    editor = os.environ.get('EDITOR')
    if editor:
        return editor

    for editor in ['nano','vim','emacs','editor','vi']:
        executable = find_executable(editor)
        if executable :
            return executable
