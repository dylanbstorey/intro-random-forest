import angreal

import os
from distutils.spawn import find_executable
import re
import subprocess
import tempfile
import datetime


from angreal.integrations.git import Git




@angreal.command()
def angreal_cmd():
    """
    update the changelog
    :return:
    """

    change_log_dir = os.path.join(os.path.dirname(__file__) , '..' , '_changelog')

    if not os.path.isdir(change_log_dir):
        exit("{} doesn't appear to exist !".format(change_log_dir))

    this_changelog_record = os.path.join(change_log_dir,
                                         datetime.datetime.today().strftime('%Y-%m-%d')+'.md')

    (fd ,path) = tempfile.mkstemp()
    with open(fd, 'w') as default:
        print('''---
title: 
type: major
date: {}
---

This release introduces

**Features:**

*

**Fixes:**

*

        '''.format(datetime.datetime.today().strftime('%Y-%m-%d')), file=default)

    editor = get_editor()
    if editor:
        subprocess.call('{} {}'.format(editor,path),shell=True)

    with open(this_changelog_record,'a') as dst:
        with open(path,'r') as src:
            print(src.read(),file=dst)

    git = Git()
    git.add(this_changelog_record)
    git.commit('-am', 'Updating changlog to reflect completed work')

    os.unlink(path)


def increment_episode(x):
    """
    increment a zero padded number by one
    """
    return str(int(x) + 1).zfill(len(x))

def get_editor():
    """
    get the system text editor
    """
    editor = os.environ.get('EDITOR')
    if editor:
        return editor

    for editor in ['nano','vim','emacs','editor','vi']:
        executable = find_executable(editor)
        if executable :
            return executable
