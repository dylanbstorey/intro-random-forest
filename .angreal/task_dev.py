import angreal
import subprocess



@angreal.command(name='dev')
def angreal_cmd():
    """
    Start the dev server
    :return:
    """
    subprocess.Popen(["jekyll", "serve", "--config", "_config.yml" ])