import os
import json
import fnmatch

import angreal
from angreal.integrations.gitlab import GitLabProject
from angreal.integrations.git import Git



@angreal.command()
@angreal.option('--gitlab_token', envvar='GITLAB_TOKEN', help='gitlab token to use (default = $GITLAB_TOKEN)')
def init(gitlab_token):
    """
    Initialize our project

    :return:
    """
    replay = get_replay()

    project = GitLabProject('https://gitlab.com', token=gitlab_token)

    project.create_project(replay.get('repo_name'))

    # Enable services
    project.enable_gitlfs()
    project.enable_issues()
    project.enable_pipelines()

    git = Git()
    git.init()
    git.remote('add', 'origin', project.project.ssh_url_to_repo)
    git.add('.')
    git.commit('-am','Angreal initialization')
    git.push('origin','master')










def get_replay():
    """
    Get the local replay
    :return:
    """

    replay = None

    for file in os.listdir( os.path.abspath(os.path.dirname(__file__))):
        if fnmatch.fnmatch(file, '*-replay.json'):
            replay = os.path.join(os.path.abspath(os.path.dirname(__file__)),file)

    assert replay

    with open(replay) as f:
        data = json.load(f)

    data = data.get('cookiecutter',None)

    return data

