---
layout: base
title: 'Setup'
---

This lesson was built using the following versions of software and libraries.

A [requirements.txt](scripts/requirements.txt) is available for download.

## graphviz

ubuntu install :
```bash
apt-get install graphviz
```
conda install :
```bash
conda install python-graphviz
```

## python

- python >3.5
- graphviz==0.8.4
- scikit-learn==0.19.2
- scipy==1.1.0
- seaborn==0.9.0
- pandas==0.23.3

Installable through conda using :
```bash
conda create -n intro-random-forests python==3.6.3
conda install scikit-learn python-graphviz
```

pip using :
```bash
pip install graphviz==0.8.4 scikit-learn==0.19.2, scipy==1.1.0
```